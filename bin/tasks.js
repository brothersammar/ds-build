#!/usr/bin/env node
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const gulp = require("gulp");
const replace = require("gulp-replace");
const del = require("del");
const vinylPaths = require("vinyl-paths");
const util = require("util");
const exec = util.promisify(require("child_process").exec);
const fs = __importStar(require("fs"));
const colors = require("colors");
const path = require("path");
const rename = require("gulp-rename");
class Tasks {
    constructor() {
        if (!fs.existsSync(process.cwd() + "/build.ds.json")) {
            throw "Arquivo build.ds.json nao encontrado.";
        }
        this.buildJson = require(process.cwd() + "/build.ds.json");
        if (this.buildJson["files_settings"]) {
            let contents = fs.readFileSync(this.buildJson["files_settings"]);
            this.filesSettings = JSON.parse(contents);
        }
        if (!this.buildJson["folder_build"]) {
            throw "Local de build nao definido.";
        }
        if (!this.buildJson["files"]) {
            throw "Arquivos que devem ser copiados nao definido.";
        }
    }
    changeFileLocation() {
        return __awaiter(this, void 0, void 0, function* () {
            let folder_build = this.buildJson["folder_build"];
            if (fs.existsSync(folder_build)) {
                console.log('TCL: Tasks -> changeFileLocation -> folder_build', folder_build);
                // Copia para pasta build para build-current
                return (new Promise((resolve, reject) => {
                    console.log(colors.green("Copiando " +
                        colors.magenta("./" + folder_build) +
                        " para " +
                        colors.magenta("./" + folder_build + "-current")));
                    gulp
                        .src([folder_build + "/**/*", folder_build + "/*"])
                        .on("error", reject)
                        .pipe(gulp.dest(folder_build + "-current"))
                        .on("end", resolve);
                })
                    // Deleta Arquivos "files" da pasta build-current
                    .then(() => {
                    console.log(colors.green("Deletando arquivos em " +
                        colors.magenta("./" + folder_build + "-current")));
                    return new Promise((resolve, reject) => {
                        this.delFiles(folder_build + "-current").then(() => {
                            resolve();
                        });
                    });
                })
                    // Copia arquivos do root para build-current
                    .then(() => {
                    return new Promise((resolve, reject) => {
                        console.log(colors.green("Copiando arquivos do root para " +
                            colors.magenta("./" + folder_build + "-current")));
                        gulp.src(this.buildJson["files"], { base: "." })
                            .on("error", reject)
                            .pipe(gulp.dest(folder_build + "-current"))
                            .on("end", resolve);
                    });
                })
                    // Trocar arquivos
                    .then(() => {
                    if (this.filesSettings) {
                        if (this.filesSettings["replaces_files"]) {
                            return new Promise((resolve, reject) => {
                                // TODO Pegar os nomes dos arquivos
                                let replaces = this.filesSettings["replaces_files"];
                                // TODO Verificar se os arquivos existem
                                for (let i = 0; i < replaces.length; i++) {
                                    const element = replaces[i];
                                    console.log('TCL: Tasks -> changeFileLocation -> element', element);
                                    // TODO Se o arquivo não existir simplesmente trocar de nome
                                    if (!fs.existsSync("./" + element.with)) {
                                        console.log(colors.red(`O arquivo ${element.with} não foi encontrado!`));
                                        // TODO Se o arquivo existir deletar o arquivo antigo e colocar o nome no novo
                                    }
                                    else if (fs.existsSync(element.with) && fs.existsSync(element.replace)) {
                                        let fileName = element.with.split('/');
                                        let folder = fileName;
                                        folder = folder.splice(folder.length - 1, 1);
                                        folder = folder.join('/');
                                        fileName = fileName[fileName.length - 1];
                                        gulp.src(folder_build + '/' + element.with)
                                            .pipe(rename(folder_build + '/' + element.replace))
                                            .pipe(gulp.dest(folder_build));
                                    }
                                }
                                // let task = gulp
                                // 	.src([folder_build + "-current/**/*"], { base: "." })
                                // 	.on("error", reject);
                            });
                        }
                    }
                    return new Promise((resolve, reject) => {
                        resolve();
                    });
                })
                    // Trocar variaveis PHP
                    .then(() => {
                    if (this.filesSettings) {
                        if (this.filesSettings["replaces_php"]) {
                            return new Promise((resolve, reject) => {
                                let replaces = this.filesSettings["replaces_php"];
                                let task = gulp
                                    .src([folder_build + "-current/**/*"], { base: "." })
                                    .on("error", reject);
                                for (let i = 0; i < replaces.length; i++) {
                                    let regex = "define\\([\"|']" + replaces[i].key + "[\"|'](.+?)\\)";
                                    let value = replaces[i].value;
                                    if (typeof value === "string") {
                                        value = '"' + value + '"';
                                    }
                                    value = 'define("' + replaces[i].key + '", ' + value + ")";
                                    task = task.pipe(replace(new RegExp(regex, "g"), value));
                                }
                                task
                                    .pipe(gulp.dest(function (file) {
                                    return file.base;
                                }))
                                    .on("end", resolve);
                            });
                        }
                    }
                    return new Promise((resolve, reject) => {
                        resolve();
                    });
                })
                    // Atualiza NPM
                    .then(() => {
                    if (this.buildJson["npm_update"]) {
                        console.log(colors.green("Atualizando NPM"));
                        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                            const { stdout, stderr } = yield exec("cd " + folder_build + "-current && npm install");
                            resolve();
                        }));
                    }
                    else {
                        return new Promise((resolve, reject) => {
                            resolve();
                        });
                    }
                })
                    // Atualiza Composer
                    .then(() => {
                    if (this.buildJson["composer_update"]) {
                        console.log(colors.green("Atualizando Composer"));
                        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                            const { stdout, stderr } = yield exec("cd " +
                                folder_build +
                                "-current && composer update --ignore-platform-reqs");
                            resolve();
                        }));
                    }
                    else {
                        return new Promise((resolve, reject) => {
                            resolve();
                        });
                    }
                })
                    // Troca pasta build-current para build
                    .then(() => {
                    console.log(colors.green("Movendo " +
                        colors.magenta("./" + folder_build + "-current") +
                        " para " +
                        colors.magenta("./" + folder_build)));
                    // Deleta pasta folder
                    if (fs.existsSync(process.cwd() + "/" + folder_build + "-old")) {
                        del.sync([process.cwd() + "/" + folder_build + "-old"]);
                    }
                    fs.renameSync(folder_build, folder_build + "-old");
                    fs.renameSync(folder_build + "-current", folder_build);
                }));
            }
            else {
                // Copia para pasta build para build-current
                return new Promise((resolve, reject) => {
                    return new Promise((resolve, reject) => {
                        gulp
                            .src(this.buildJson["files"], { base: "." })
                            .on("error", reject)
                            .pipe(gulp.dest(folder_build))
                            .on("end", resolve);
                    });
                });
            }
        });
    }
    /**
     * Deletar Arquivos
     * @param {string} path [description]
     */
    delFiles(path) {
        let files = [];
        this.buildJson["files"].forEach((element) => {
            files.push(path + "/" + element);
        });
        return del(files);
    }
    teste() {
        console.log("teste");
    }
}
exports.Tasks = Tasks;
const task = new Tasks();
task.changeFileLocation();
